# calico

This is repository contains the Calico configuration that we deploy in Wikimedia Toolforge.

See also: https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Kubernetes/Components

As of this writing, upstream provides a helm chart for calico that we don't use, see:
https://docs.tigera.io/archive/v3.21/getting-started/kubernetes/helm
