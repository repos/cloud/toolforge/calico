apiVersion: v1
kind: ServiceAccount
metadata:
  name: calicoctl
  namespace: kube-system
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: calicoctl
  namespace: kube-system
spec:
  replicas: 1
  selector:
     matchLabels:
       name: calicoctl
       k8s-app: calicoctl
  template:
    metadata:
       name: calicoctl
       namespace: kube-system
       labels:
         name: calicoctl
         k8s-app: calicoctl
    spec:
      nodeSelector:
        beta.kubernetes.io/os: linux
      hostNetwork: true
      serviceAccountName: calicoctl
      containers:
      - name: calicoctl
        image: {{ .Values.registry }}/calico/ctl:{{ .Values.calicoVersion }}
        command:
          - /usr/bin/calicoctl
        args:
          - version
          - --poll=1m
        env:
        - name: DATASTORE_TYPE
          value: kubernetes
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: calicoctl
rules:
  - apiGroups: [""]
    resources:
      - namespaces
      - nodes
    verbs:
      - get
      - list
      - update
  - apiGroups: [""]
    resources:
      - nodes/status
    verbs:
      - update
  - apiGroups: [""]
    resources:
      - pods
      - serviceaccounts
    verbs:
      - get
      - list
  - apiGroups: [""]
    resources:
      - pods/status
    verbs:
      - update
  - apiGroups: ["crd.projectcalico.org"]
    resources:
      - bgppeers
      - bgpconfigurations
      - clusterinformations
      - felixconfigurations
      - globalnetworkpolicies
      - globalnetworksets
      - ippools
      - networkpolicies
      - networksets
      - hostendpoints
      - ipamblocks
      - blockaffinities
      - ipamhandles
    verbs:
      - create
      - get
      - list
      - update
      - delete
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: calicoctl
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: calicoctl
subjects:
- kind: ServiceAccount
  name: calicoctl
  namespace: kube-system
